install:
	pipenv install

migrations:
	python3 manage.py makemigrations

migrate:
	python3 manage.py migrate

superuser:
	python3 manage.py createsuperuser

collectstatic:
	python3 manage.py collectstatic

shell:
	python3 manage.py shell_plus

set_env_vars:
	@[ -f .env ] && source .env

serve:
	python3 manage.py runserver

.PHONY: set_env_vars

docker_admin:
	docker-compose -f docker-compose.yml exec web python manage.py createsuperuser  --no-input --clear
docker_migrate:
	docker-compose -f docker-compose.yml exec web python manage.py migrate  --no-input --clear
docker_up_d:
	docker-compose up --build -d
docker_up:
	docker-compose up --build
	
