# Django Food Ordering App

This Django application allows users to place food orders, manage products, and track order progress. The app includes three roles: Restaurant Manager, User, and Chef, each with specific functionalities.

## Features

- **Restaurant Manager:**
  - Add products: Ability to add new food items to the menu with details such as name, description, price, and image.
  - Manage orders: View and manage incoming orders, update their status, and mark them as completed.

- **User:**
  - Place orders.
  - Track order progress: View the status of placed orders (e.g., pending, in progress, completed).

- **Chef:**
  - Update order status: Mark orders as in progress and complete once food preparation is done (Only chef can mark an order as complete).

## API Endpoints

- **List and Create Orders:**
  - `GET /api/orders/`: Retrieve a list of orders.
  - `POST /api/orders/`: Create a new order.

- **Retrieve, Update, and Delete Order:**
  - `GET /api/orders/<order_id>/`: Retrieve details of a specific order.
  - `PUT /api/orders/<order_id>/`: Update details of a specific order.
  - `DELETE /api/orders/<order_id>/`: Delete a specific order.


- **List and Create Products:**
  - `GET /api/products/`: Retrieve a list of products.
  - `POST /api/products/`: Create a new product.

- **Retrieve, Update, and Delete Product:**
  - `GET /api/products/<product_id>/`: Retrieve details of a specific product.
  - `PUT /api/products/<product_id>/`: Update details of a specific product.
  - `DELETE /api/products/<product_id>/`: Delete a specific product.
### Order Model

Represents an order placed by a user.

- `user`: The user who placed the order.
- `products`: The products included in the order.
- `order_status`: The status of the order (e.g., 'Pending', 'In Progress', 'Completed').
- `ordered_items`: Details about each ordered item.
- `order_total_price`: The total price of the order.
- `order_placed`: Indicates whether the order has been placed.
- `order_number`: A unique identifier for the order.

### Product Model

Represents a food product available for order.

- `name`: The name of the product.
- `description`: The description of the product.
- `price`: The price of the product.
- `image`: The image representing the product.
- `total_order_placed`: The total number of orders placed for the product.
- `is_available`: Indicates whether the product is currently available.
- `delivery_time`: The cooking time required for the product.

- **Chef Update Order Status:**
  - `PUT /api/chef/update-order-status/<order_id>/`: Update the status of a specific order by a chef.


## Setup using Pipenv

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/test9304009/backend-test.git
   cd backend-test
   ```

2. Install Pipenv if not already installed:
   ```bash
   pip install pipenv
   ```

3. Set up a virtual environment and Activate the virtual environment:
   ```bash
   pipenv shell
   ```

4. Install dependencies using Pipenv:
   ```bash
   pipenv install
   ```

## Database Setup

1. Create a new PostgreSQL database for the project. 

2. Update the `.env_sample` file with your database credentials:
   ```env
   # .env.sample
DATABASE_URL='postgresql://localhost/<your-databasename>?password=<your-password>'

   ```

3. Save the changes and rename the `.env.sample` file to `.env`.

## Migrations and Superuser

1. Apply migrations to create database tables:
   ```bash
   make migrate
   ```

2. Create a superuser for admin access:
   ```bash
   make createsuperuser
   ```

## Running the Development Server

1. Run the development server:
   ```bash
   make serve
   ```

2. Access the admin panel at `http://localhost:8000/admin/` to manage products, orders, and other app data.  

3. You can access the endpoint either through this Postman collection [link](https://api.postman.com/collections/4791058-361b6c7f-d075-4fc9-ac25-bf4a2f607fa0?access_key=PMAT-01HQVADTM844271V5KVXNW6D1H) or by utilizing Swagger at http://localhost:8000/swagger/.





## Contributors

- Your Name kibet_

Feel free to contribute, report issues, or provide feedback. Happy coding!

