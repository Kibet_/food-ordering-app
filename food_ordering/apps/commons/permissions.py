# permissions.py
from rest_framework import permissions

class IsRestaurantManagerOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        # Allow read permissions for all requests
        if request.method in permissions.SAFE_METHODS:
            return True

        # Check if the user has the 'Restaurant Manager' role
        return request.user.is_authenticated and request.user.type == 'RESTAURANT_MANAGER'
