# urls.py
from django.urls import path
from . import views
urlpatterns = [
    path('orders/', views.OrderListCreateAPIView.as_view(), name='order-list-create'),
    path('orders/<int:pk>/', views.OrderRetrieveUpdateDestroyAPIView.as_view(), name='order-retrieve-update-destroy'),
    path('chef/update-order-status/<int:order_id>/', views.ChefUpdateOrderStatusAPIView.as_view(), name='chef-update-order-status'),

]
