from django.db import models
from food_ordering.apps.commons.base import CommonFieldsMixin
from food_ordering.apps.authentication.models import User
from food_ordering.apps.restaurants.models import Product
from django.utils import timezone

# Create your models here.
import uuid

class Order(CommonFieldsMixin):
    """
    Model representing an order.

    Attributes:
        user (ForeignKey): The user who placed the order.
        products (ManyToManyField): The products included in the order.
        order_status (CharField): The status of the order (e.g., 'Pending', 'In Progress', 'Completed').
        ordered_items (JSONField): Details about each ordered item.
        order_total_price (CharField): The total price of the order.
        order_placed (BooleanField): Indicates whether the order has been placed.
        order_number (CharField): A unique identifier for the order.
    """    
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    products = models.ManyToManyField(Product)
    order_status = models.CharField(max_length=20, choices=[('Pending', 'Pending'), ('In Progress', 'In Progress'), ('Completed', 'Completed')])
    ordered_items = models.JSONField()
    order_total_price = models.CharField(max_length=500, default=0, blank=True, null=True)
    order_placed = models.BooleanField(default=False, null=True, blank=False)
    order_number = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    
    def is_stale(self):
        # Check if the order is stale (placed more than 24 hours ago)
        elapsed_time = timezone.now() - self.created_at
        return elapsed_time.total_seconds() > 24 * 60 * 60