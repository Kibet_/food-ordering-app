# serializers.py
from rest_framework import serializers
from .models import Order
from food_ordering.apps.restaurants.serializers import ProductSerializer  # Assuming you have a ProductSerializer

class OrderSerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True, read_only=True)

    class Meta:
        model = Order
        fields = '__all__'
