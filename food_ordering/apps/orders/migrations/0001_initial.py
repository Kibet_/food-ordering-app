# Generated by Django 5.0.2 on 2024-02-29 15:12

import django.db.models.deletion
import django.db.models.manager
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("restaurants", "0003_alter_product_name"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name="Order",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created_at", models.DateTimeField(auto_now_add=True)),
                ("updated_at", models.DateTimeField(auto_now=True)),
                (
                    "deleted",
                    models.BooleanField(
                        default=False,
                        help_text="This is to make sure deletes are not actual deletes",
                    ),
                ),
                ("is_active", models.BooleanField(default=True)),
                (
                    "order_status",
                    models.CharField(
                        choices=[
                            ("Pending", "Pending"),
                            ("In Progress", "In Progress"),
                            ("Completed", "Completed"),
                        ],
                        max_length=20,
                    ),
                ),
                ("ordered_items", models.JSONField()),
                (
                    "order_total_price",
                    models.CharField(blank=True, default=0, max_length=500, null=True),
                ),
                ("order_placed", models.BooleanField(default=False, null=True)),
                ("products", models.ManyToManyField(to="restaurants.product")),
                (
                    "user",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={
                "ordering": ["-updated_at", "-created_at"],
                "abstract": False,
            },
            managers=[
                ("everything", django.db.models.manager.Manager()),
            ],
        ),
    ]
