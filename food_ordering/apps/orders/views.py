# views.py
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Order
from .serializers import OrderSerializer
from rest_framework.permissions import IsAuthenticated
class OrderListCreateAPIView(APIView):
    serializer_class = OrderSerializer
    permission_classes = [IsAuthenticated]    
    def get(self, request, format=None):
        """
        Retrieve a list of orders.

        Args:
            request: The request object.
            format: The format of the response data (default is None).

        Returns:
            Response: A response containing the list of orders.
        """
        orders = Order.objects.all()
        serializer = OrderSerializer(orders, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        """
        Create a new order.

        Args:
            request: The request object.
            format: The format of the response data (default is None).

        Returns:
            Response: A response containing the created order.
        """        
        serializer = OrderSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class OrderRetrieveUpdateDestroyAPIView(APIView):
    """
    API view for retrieving, updating, and deleting a specific order.

    GET:
        Retrieve details of a specific order.

    PUT:
        Update details of a specific order.

    DELETE:
        Delete a specific order.
    """
    def get_object(self, pk):
        try:
            return Order.objects.get(pk=pk)
        except Order.DoesNotExist:
            return Response({'error': 'Order not found.'}, status=status.HTTP_404_NOT_FOUND)

    def get(self, request, pk, format=None):
        """
        Retrieve details of a specific order.

        Args:
            request: The request object.
            pk: The unique identifier of the order.
            format: The format of the response data (default is None).

        Returns:
            Response: A response containing the details of the order.
        """        
        order = self.get_object(pk)
        serializer = OrderSerializer(order)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        """
        Update details of a specific order.

        Args:
            request: The request object.
            pk: The unique identifier of the order.
            format: The format of the response data (default is None).

        Returns:
        """
        order = self.get_object(pk)
       # Exclude 'order_status' field from the data before passing it to the serializer
        request_data = {key: value for key, value in request.data.items() if key != 'order_status'}
                
        serializer = OrderSerializer(order, data=request_data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        """
        Delete a specific order.

        Args:
            request: The request object.
            pk: The unique identifier of the order.
            format: The format of the response data (default is None).

        Returns:
            Response: A response indicating the success of the deletion.
        """
        order = self.get_object(pk)
        order.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ChefUpdateOrderStatusAPIView(APIView):
    """
    API view for allowing chefs to update the status of an order.

    PUT:
        Update the status of a specific order by a chef.
    """
    def get_object(self, order_id):
        try:
            return Order.objects.get(id=order_id)
        except Order.DoesNotExist:
            return Response({'error': 'Order not found.'}, status=status.HTTP_404_NOT_FOUND)

    def put(self, request, order_id, format=None):
        order = self.get_object(order_id)
        
        # Check if the user making the request is a chef
        if not request.user.type == 'CHEF':
            return Response({'error': 'Only chefs can update order status.'}, status=status.HTTP_403_FORBIDDEN)

        # Check if the order is already marked as completed
        if order.order_status == 'Completed':
            return Response({'error': 'Order is already marked as completed.'}, status=status.HTTP_400_BAD_REQUEST)

        # Update the order status based on the request data
        order_status = order.get('order_status', None)
        if order_status in ['In Progress', 'Completed']:
            order.order_status = order_status
            order.save()
            serializer = OrderSerializer(order)
            return Response(serializer.data)
        else:
            return Response({'error': 'Invalid order status.'}, status=status.HTTP_400_BAD_REQUEST)
