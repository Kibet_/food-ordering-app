from django.contrib import admin

from django.contrib.auth.admin import UserAdmin

from .models import Chef,RestaurantManager,StandardUser,User

class StaffAdmin(UserAdmin):
    pass

class SupervisorAdmin(UserAdmin):
    pass
# Register your models here.
admin.site.register(User,StaffAdmin)
admin.site.register(StandardUser,StaffAdmin)
admin.site.register(Chef,SupervisorAdmin)
admin.site.register(RestaurantManager,SupervisorAdmin)
