from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _
from rest_framework_simplejwt.tokens import RefreshToken

from food_ordering.apps.commons.base import CommonFieldsMixin


class User(AbstractUser, CommonFieldsMixin):
    """Base class for all users"""

    is_verified = models.BooleanField(
        default=True,
    )
    phone_number = models.CharField(max_length=13, default=0)
    password = models.CharField(max_length=128, blank=True)
    full_name = models.CharField(max_length=128, blank=True)


    class Types(models.TextChoices):
        """User types"""

        ADMIN = "ADMIN", "Admin- Careful here!"
        RESTAURANT_MANAGER= "RESTAURANT_MANAGER", "Restaurant Manager"
        STANDARD_USER = "STANDARD_USER","Standard User"
        CHEF = "CHEF","Chef"

    base_type = Types.ADMIN

    type = models.CharField(
        _("Type"), max_length=50, choices=Types.choices, default=base_type
    )
    email = models.CharField(_("email of User"), unique=True, max_length=255)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.type = self.base_type

        return super().save(*args, **kwargs)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["username"]

    def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {"refresh": str(refresh), "access": str(refresh.access_token)}


""" =================== Proxy Model Managers ================= """


class RestaurantManManager(models.Manager):
    def get_queryset(self, *args, **kwargs):
        return super().get_queryset(*args, **kwargs).filter(type=User.Types.RESTAURANT_MANAGER)

    @classmethod
    def normalize_email(cls, email):
        """
        Normalize the email address by lowercasing the domain part of the it.
        """
        email = email or ""
        try:
            email_name, domain_part = email.strip().rsplit("@", 1)
        except ValueError:
            pass
        else:
            email = "@".join([email_name, domain_part.lower()])
        return email


class StandardUserManager(models.Manager):
    def get_queryset(self, *args, **kwargs):
        return super().get_queryset(*args, **kwargs).filter(type=User.Types.STANDARD_USER)

    @classmethod
    def normalize_email(cls, email):
        """
        Normalize the email address by lowercasing the domain part of the it.
        """
        email = email or ""
        try:
            email_name, domain_part = email.strip().rsplit("@", 1)
        except ValueError:
            pass
        else:
            email = "@".join([email_name, domain_part.lower()])
        return email


class ChefManager(models.Manager):
    def get_queryset(self, *args, **kwargs):
        return super().get_queryset(*args, **kwargs).filter(type=User.Types.CHEF)

    @classmethod
    def normalize_email(cls, email):
        """
        Normalize the email address by lowercasing the domain part of the it.
        """
        email = email or ""
        try:
            email_name, domain_part = email.strip().rsplit("@", 1)
        except ValueError:
            pass
        else:
            email = "@".join([email_name, domain_part.lower()])
        return email

""" ============================== Proxy Models =================================== """


class RestaurantManager(User):
    """class to create RestaurantManager objects & associated attributes"""

    base_type = User.Types.RESTAURANT_MANAGER
    objects = RestaurantManManager()

    def save(self, *args, **kwargs):
        if not self.pk:
            self.type = User.Types.RESTAURANT_MANAGER
            if self.password:
                # If password is provided, hash it using set_password
                self.set_password(self.password)
        return super().save(*args, **kwargs)

    class Meta:
        proxy = True
        ordering = ["-created_at", "-updated_at"]
class StandardUser(User):
    """Class to create StandardUser objects & associated attributes"""

    base_type = User.Types.STANDARD_USER
    objects = StandardUserManager()

    def save(self, *args, **kwargs):
        if not self.pk:
            self.type = User.Types.STANDARD_USER
            if self.password:
                # If password is provided, hash it using set_password
                self.set_password(self.password)
        return super().save(*args, **kwargs)

    class Meta:
        proxy = True


class Chef(User):
    """Class to create Chef objects & associated attributes"""

    base_type = User.Types.CHEF
    objects = ChefManager()

    def save(self, *args, **kwargs):
        if not self.pk:
            self.type = User.Types.CHEF
            if self.password:
                # If password is provided, hash it using set_password
                self.set_password(self.password)
        return super().save(*args, **kwargs)

    class Meta:
        proxy = True


