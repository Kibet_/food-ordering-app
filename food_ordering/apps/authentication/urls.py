from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from . import views

app_name = "authentication"
urlpatterns = [
    path("auth/registration/<user_id>",views.SingleUserRegistrationView.as_view(),name="SingleUserRegistration",),
    path("auth/login/", views.SigninView.as_view(), name="token_obtain_pair"),
    path("auth/token/refresh", TokenRefreshView.as_view(), name="token_refresh"),
    path("auth/logout/", views.SignoutView.as_view(), name="SignoutView"),
]
