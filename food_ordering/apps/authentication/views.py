from django.db import models  # Add this line
import json
from datetime import datetime, timedelta

from django.contrib.auth import authenticate
from django.core import serializers as token_serializer
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import generics, status
from rest_framework.authtoken.models import Token
from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.exceptions import TokenError
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from food_ordering.apps.authentication.models import User,Chef,RestaurantManager,StandardUser
from food_ordering.apps.authentication.serializers import (
    CustomTokenObtainPairSerializer,
    SignOutSerializer,
    UserRegistrationSerializer,
    UserRegistrationSerializerView
)



# Create your views here.
class SingleUserRegistrationView(ListCreateAPIView):
    """
    single user account creation
    Args:
        first_name: the user's first name
        last_name: the user's last name
        username: username that will be indexed by the system
        email: the user's email
        password: The new account password
        role: The user's role either Chef,Restaurant Manager,User
    """

    permission_classes = [AllowAny]
    serializer_class = UserRegistrationSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer_data = serializer.validated_data
        serializer.save()
        message={"status":"Account has been created successfully"}
        return Response(message, status=status.HTTP_201_CREATED)

    def put(self, request, user_id=None):
        try:
            user = User.objects.get(id=int(user_id))
        except User.DoesNotExist:
            return Response(
                {"error": f"Check user details"}, status=status.HTTP_404_NOT_FOUND
            )

        serializer = self.serializer_class(
            instance=user, data=request.data, partial=True, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        serializer_data = serializer.validated_data
        # serializer.save()
        user = User.objects.filter(id=request.user.id).update(**serializer_data)
        return Response(
            {"success": "Profile has been updated successfully"}, status=status.HTTP_200_OK
        )



class SigninView(TokenObtainPairView):
    permission_classes = (AllowAny,)
    serializer_class = CustomTokenObtainPairSerializer



class CustomRefreshTokenView(TokenRefreshView):
    permission_classes = (AllowAny,)
    serializer_class = CustomTokenObtainPairSerializer



class SignoutView(generics.CreateAPIView):
    """Adds a token to the blacklist table upon signout

    Arguments:
        refresh_token {str} -- A valid refresh token generated via login/signup endpoints
    """

    serializer_class = SignOutSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        token = request.data.get("refresh")
        success_message = {
            "success": "User logged out successfully",
        }
        error_message = {
            "error": "Token is invalid or expired",
        }
        try:
            token = RefreshToken(token)
            token.blacklist()

            return Response(success_message, status=status.HTTP_200_OK)

        except TokenError as error:
            return Response(error_message, status=status.HTTP_400_BAD_REQUEST)

