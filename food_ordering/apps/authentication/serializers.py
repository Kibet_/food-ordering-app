import enum
from tokenize import blank_re

from django.contrib.auth import authenticate
from django.contrib.auth.hashers import make_password
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.tokens import RefreshToken
from .models import User,Chef,StandardUser,RestaurantManager


class UserRegistrationSerializer(serializers.ModelSerializer):
    class Roles(enum.Enum):
        RESTAURANT_MANAGER= "RESTAURANT_MANAGER", "Restaurant Manager"
        STANDARD_USER = "STANDARD_USER","Standard User"
        CHEF = "CHEF","Chef"

    password = serializers.CharField(
        min_length=4, required=False, max_length=100, write_only=True
    )
    roles = [role.value for role in Roles]
    type = serializers.ChoiceField(choices=roles, required=True)
    phone_number = serializers.CharField(
        max_length=13,
        allow_blank=True,
        required=False,
        validators=[UniqueValidator(queryset=User.objects.all())],
    )
    username = serializers.CharField(
        max_length=100,
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())],
    )
    full_name = serializers.CharField(max_length=100, required=True)
    email = serializers.CharField(
        max_length=100,
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())],
    )

    class Meta:
        model = User
        fields = [
            "full_name",
            "username",
            "email",
            "password",
            "type",
            "phone_number",
        ]

        extra_kwargs = {"password": {"write_only": True}}

    def create(self, validated_data):
        role = self.validated_data["type"]
        if role == "STANDARD_USER":
            try:
                user = StandardUser.objects.create(
                    username=validated_data["username"],
                    full_name=validated_data["full_name"],
                    email=validated_data["email"],
                    type=role,
                    phone_number=validated_data.get("phone_number"),
                    password = validated_data.get("password",None)

                )
            except Exception as err:
                raise serializers.ValidationError(
                    {"error": f"{err}"}
                )

        elif role == "CHEF":
            try:
                user = Chef.objects.create(
                    username=validated_data["username"],
                    full_name=validated_data["full_name"],
                    email=validated_data["email"],
                    type=role,
                    phone_number=validated_data.get("phone_number"),
                    password = validated_data.get("password",None)
                )
                    
                

            except Exception as err:
                raise serializers.ValidationError({"error": f"{err}"})
            

        elif role == "RESTAURANT_MANAGER":
            try:
                # import pdb;pdb.set_trace()
                user = RestaurantManager.objects.create(
                    username=validated_data["username"],
                    full_name=validated_data["full_name"],
                    email=validated_data["email"],
                    type=role,
                    phone_number=validated_data.get("phone_number"),
                    password = validated_data.get("password",None)
                )
                    
                

            except Exception as err:
                raise serializers.ValidationError({"error": f"{err}"})

        return user


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):

    def validate(self, attrs):

        try:
            if User.objects.get(email=attrs.get("email")).is_verified == False:
                # if this is processed then redirect this user to the password activation page
                return {"error": "Account is inactive"}
            return super().validate(attrs)
        except Exception as err:
            return {"error": f"{err}"}

    @classmethod
    def get_token(cls, user):        
        token = super().get_token(user)
        token["role"] = user.type
        token["username"] = user.username
        return token


class SignOutSerializer(serializers.Serializer):
    refresh_token = serializers.CharField(required=True)




class UserRegistrationSerializerView(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = (
            "first_name",
            "last_name",
            "first_time_user",
            "groups",
            "user_permissions",
            "password",
            "is_active",
            "is_superuser",
            "last_login",
        )


class CustomUserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "username")

