# urls.py
from django.urls import path
from .views import ProductListCreateAPIView, ProductRetrieveUpdateDestroyAPIView
app_name = "restaurant"

urlpatterns = [
    path('restaurant/products/', ProductListCreateAPIView.as_view(), name='product-list-create'),
    path('restaurant/products/<int:pk>/', ProductRetrieveUpdateDestroyAPIView.as_view(), name='product-retrieve-update-destroy'),
]
