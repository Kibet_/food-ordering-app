from django.db import models


class Product(models.Model):
    """
    Model representing a product.

    Attributes:
        name (CharField): The name of the product.
        description (TextField): The description of the product.
        price (DecimalField): The price of the product.
        image (ImageField): The image representing the product.
        total_order_placed (IntegerField): The total number of orders placed for the product.
        available_servings (IntegerField) : Indicates the quantity available.
        delivery_time (CharField): The cooking time required for the product.
    """
    name = models.CharField(max_length=255,unique=True)
    description = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    image = models.ImageField(upload_to='product_images/')
    total_order_placed = models.IntegerField(default = 0, null=True, blank=True)
    delivery_time = models.CharField(max_length=10, null=True, blank=True, verbose_name="Cooking Time")
    available_servings = models.IntegerField(default=0)

