
# views.py
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Product
from .serializers import ProductSerializer
from food_ordering.apps.commons.permissions import IsRestaurantManagerOrReadOnly
class ProductListCreateAPIView(APIView):
    """
    API view for listing and creating products.

    GET:
        Retrieve a list of products.

    POST:
        Create a new product.
    """

    permission_classes = [IsRestaurantManagerOrReadOnly]

    def get(self, request, format=None):
        """
        Retrieve a list of products.

        Args:
            request: The request object.
            format: The format of the response data (default is None).

        Returns:
            Response: A response containing the list of products.
        """        
        products = Product.objects.all()
        serializer = ProductSerializer(products, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        """
        Create a new product.

        Args:
            request: The request object.
            format: The format of the response data (default is None).

        Returns:
            Response: A response containing the created product.
        """
        serializer = ProductSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ProductRetrieveUpdateDestroyAPIView(APIView):
    """
    API view for retrieving, updating, and deleting a specific product.

    GET:
        Retrieve details of a specific product.

    PUT:
        Update details of a specific product.

    DELETE:
        Delete a specific product.
    """
    def get_object(self, pk):
        try:
            return Product.objects.get(pk=pk)
        except Exception as err:
            raise Response(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, pk, format=None):
        """
        Retrieve details of a specific product.

        Args:
            request: The request object.
            pk: The unique identifier of the product.
            format: The format of the response data (default is None).

        Returns:
            Response: A response containing the details of the product.
        """
        try:

            product = Product.objects.get(id=pk)
            serializer = ProductSerializer(product)
            
            return Response(serializer.data)
        except Exception as err:
            return Response(f'{err}', status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        """
        Update details of a specific product.

        Args:
            request: The request object.
            pk: The unique identifier of the product.
            format: The format of the response data (default is None).

        Returns:
            Response: A response containing the updated product details.
        """
        try:

            product = Product.objects.get(id=pk)
            serializer = ProductSerializer(product, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as err:
            return Response(f'{err}', status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        """
        Delete a specific product.

        Args:
            request: The request object.
            product_id: The unique identifier of the product.
            format: The format of the response data (default is None).

        Returns:
            Response: A response indicating the success of the deletion.
        """
        try:

            product = Product.objects.get(id=pk)
            product.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
            
        except Exception as err:
            return Response(f'{err}', status=status.HTTP_400_BAD_REQUEST)
