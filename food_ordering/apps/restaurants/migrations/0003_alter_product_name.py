# Generated by Django 5.0.2 on 2024-02-29 13:03

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("restaurants", "0002_remove_product_is_available_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="product",
            name="name",
            field=models.CharField(max_length=255, unique=True),
        ),
    ]
